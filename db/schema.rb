# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140821080959) do

  create_table "hashtags", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "n_view",     default: 0
  end

  create_table "hashtags_histories", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "hashtag_id"
  end

  create_table "hashtags_shows", force: true do |t|
    t.integer "show_id"
    t.integer "hashtag_id"
  end

  create_table "shows", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.date     "release_date"
    t.string   "img"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "id_betaserie"
    t.integer  "episodes"
    t.integer  "seasons"
  end

end
