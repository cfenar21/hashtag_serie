class ChangeAssociationTableHAshtagHistory < ActiveRecord::Migration
	def change
		remove_column :hashtags, :history_id
		add_column :hashtags_history, :hashtag_id, :integer
	end
end
