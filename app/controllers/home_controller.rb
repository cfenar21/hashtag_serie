class HomeController < ApplicationController
	def index
		# Recherche
		@q = Show.search(params[:q])
		
		# On récupère les 10 hashtags les plus populaires
		@most_popular_hashtag = Hashtag.order(n_view: :desc).take(10)
		
		# On récupère 24 images de séries aléatoirement
		@shows_img = Show.select(:img).where("img IS NOT NULL").order("RANDOM()").limit(24)
		puts @shows_img
		
		# Ajout du titre
		@title = "Accueil"
	end
end
