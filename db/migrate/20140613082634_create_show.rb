class CreateShow < ActiveRecord::Migration

  def change
    create_table :shows do |t|
      t.string :title
      t.text :description
      t.date :release_date
      t.string :img
      t.timestamps
    end
  end
end
