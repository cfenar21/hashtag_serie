module ApplicationHelper
	def bootstrap_class(key)
		hash = {
			notice: "success",
			error: "danger",
			warning: "warning"
		}

		hash[key]
	end
	
	def render_title
	  return @title + " - #Séries" if defined?(@title)
	  "#Séries"
	end
end
