class AddColumnEpisodeAndSeason < ActiveRecord::Migration
  def change
  	  	add_column :shows, :episodes, :integer
  		add_column :shows, :seasons, :integer

  end
end
