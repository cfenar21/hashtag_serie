class AddTableHAshtagHistory < ActiveRecord::Migration
  def change
	
	create_table :hashtags_history do |t|
      	t.timestamps
    end
 
    add_column :hashtags, :history_id, :integer
    
  end
end
