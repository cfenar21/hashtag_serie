namespace :serie do
  	desc "TODO"
  	# Taper rake serie:update_all[maj_serie] pour lancer la tâche
  	task :update_all, [:arg_type]  => :environment do |t,args|
  		require 'net/http'
  		require 'json'
  	
  		key = "957D53EAA243"
  		
  		all_serie = Net::HTTP.get(URI.parse('http://api.betaseries.com/shows/list?&key=' + key))
		all_serie = JSON.parse(all_serie)
		all_serie = all_serie["shows"]
		
		#Show.destroy_all		
		
		all_serie.each do |as|
			serie = Show.find_by_id_betaserie(as['id'])

			#if serie
			if serie.present?
				puts args.arg_type
				if args.arg_type == 'maj_serie'
					description_serie = Net::HTTP.get(URI.parse('http://api.betaseries.com/shows/display?id=' + as['id'] + '&key='+key))
					description_serie = JSON.parse(description_serie)
					description_serie = description_serie['show']
					season_serie = description_serie['seasons']
					episode_serie = description_serie['episodes']

					image_serie = Net::HTTP.get(URI.parse('http://api.betaseries.com/shows/pictures?id=' + as['id'] + '&key='+key))
					image_serie = JSON.parse(image_serie)
					image_serie = image_serie['pictures']
					image_serie.take(1).each do |img|
						serie.update({title: as['title'], description: description_serie['description'],img: img['url'],seasons: season_serie, episodes: episode_serie})
					end
						puts "serie deja existante, mise a jour: " + serie.title
				else
					puts "serie deja existante : " + serie.title
				end
			else
				description_serie = Net::HTTP.get(URI.parse('http://api.betaseries.com/shows/display?id=' + as['id'] + '&key='+key))
				description_serie = JSON.parse(description_serie)
				description_serie = description_serie['show']
			
				series = Show.new(:title => as['title'],:description => description_serie['description'], :id_betaserie => as['id'].to_i)
				if series.save
					puts "nouvelle serie : " + as['title']
				end
			end	
		end
 	end
end
