class Correction < ActiveRecord::Migration
  def change
	add_column :hashtags_shows, :show, :belongs_to

	remove_column :hashtags, :show_id

  end
end
