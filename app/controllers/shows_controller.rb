class ShowsController < ApplicationController
	
	add_breadcrumb 'Accueil', '/'
	add_breadcrumb 'Séries', 'shows_path'
	
	def index
		# Lorsqu'on fait une recherche depuis la page d'accueil
		@q = Show.search(params[:q])
		if !params[:q].nil?
		 	hash_searched = Hashtag.find_by_title(params[:q][:hashtags_title_cont])
		 	if !hash_searched.nil?
		 		Hashtag.increment_counter(:n_view, 1)
		 		hash_searched.increment(:n_view)
		 		hash_searched.save
		 	end
		end
		@show = @q.result.includes(:hashtags).page params[:page]
		
		@nb_shows = Show.count
		
		@search_query = params[:q]
		
		# Ajout du titre
		@title = "Toutes les séries"
	end

	def show
		# On va chercher la série via son paramètre
		@this_show = Show.find(params[:id])
		
		# On instance l'objet Hashtag pour le formulaire d'ajout
		@new_hash = Hashtag.new
		
		# Ajout du titre
		@title = @this_show.title
		
		# Ajout du breadcrumb
		add_breadcrumb @this_show.title, ''
	end

	def create
	end
end