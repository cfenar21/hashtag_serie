class AddHitoriesAndnviewDefaultValue < ActiveRecord::Migration
  def change
  	rename_table(:hashtags_history, :hashtags_histories)
  	change_column :hashtags, :n_view, :integer, :default => 0
  end
end
