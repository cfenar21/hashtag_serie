
$(document).ready(function(){


var ctx = $("#myChart").get(0).getContext("2d");
var data = {
    labels: ["2014-07-01", "2014-07-02"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [1, 1]
        },
    ]
};

var myLineChart = new Chart(ctx).Line(data);
});