class AddTableHastags < ActiveRecord::Migration
  	def change
  		create_table :hashtags do |t|
			t.string :title
			t.integer :show_id
			t.timestamps
		end


		create_table :hashtags_shows do |t|
			t.belongs_to :show
			t.belongs_to :hashtag
    	end
  	end
end
