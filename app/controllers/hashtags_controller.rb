class HashtagsController < ApplicationController
	
	add_breadcrumb 'Accueil', '/'
	add_breadcrumb 'Hashtags', 'hashtags_path'
	add_breadcrumb 'Create a new thing', '', :only => [:create]
	
	def index
		@last_hashtag = Hashtag.order(id: :asc).last(10)
		@new_hash = Hashtag.new
		# Ajout du titre
		@title = "Tous les Hashtags"
	end

	def create
		param_new_hash = Hashtag.find_by_title(params_hash[:title])
		if !param_new_hash.nil?
			existing_serie = param_new_hash.shows
		end
		# hashtag inexistant et presence d'un id serie (donc non associé) => creation du hashtag et de l'association

		if param_new_hash.nil? && !params_hash[:shows].nil?
			new_hash = Hashtag.new(:title => params_hash[:title])
			new_hash.save
	 		show_associated = Show.find(params_hash[:shows])
			new_hash.shows << show_associated
			flash[:notice] = 'Votre Hashtag a été créé et associé à cette série !'
			redirect_to show_path(params_hash[:shows])
		
		# hashtag deja existant et presence d'un id serie & association non existant => creation de l'association
		elsif !param_new_hash.nil? && !params_hash[:shows].nil? && !existing_serie.exists?
			new_hash = Hashtag.find_by_title(params_hash[:title])
	 		show_associated = Show.find(params_hash[:shows])
			new_hash.shows << show_associated
			flash[:notice] = 'Votre Hashtag a été associé à cette série !'
			redirect_to show_path(params_hash[:shows])
		
		# hashtag deja existant et presence d'un id serie & association deja existante => erreur
		elsif !param_new_hash.nil? && !params_hash[:shows].nil? && existing_serie.exists?
			flash[:warning] = 'Cet Hashtag est déjà associé à cette série !'
			redirect_to show_path(params_hash[:shows])
		# hashtag inexistant et pas de serie renseigné => creation d'un hashtag
		elsif param_new_hash.nil? && params_hash[:shows].nil?
			new_hash = Hashtag.new(:title => params_hash[:title])
			new_hash.save
			flash[:notice] = 'Votre Hashtag a été créé !'
			redirect_to hashtags_path
		# hashtag deja existant et pas de serie renseigné => erreur
		elsif !param_new_hash.nil? &&  params_hash[:shows].nil?
			flash[:error] = 'Cet Hashtag existe déjà !'
			redirect_to hashtags_path
		end
	end

	def show
		# On affiche le hashtag correspondant à l'id passé en paramètre GET
		@hashtag = Hashtag.find(params[:id])
		# On incrémente le nombre de vue de cet hashtag
		@hashtag.update_attribute(:n_view, @hashtag.n_view+1)
		# Ajout du titre
		@title = "#"+@hashtag.title
		# Ajout du breadcrumb
		add_breadcrumb @hashtag.title, ''
	end

	private

 	def params_hash
  		params[:hashtag].permit(:title,	:shows) 
  	end

end
